set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR aarch64)

set(CMAKE_SYSROOT /ssh_sysroot)
set(CMAKE_FIND_ROOT_PATH /ssh_sysroot)

#set(CMAKE_SYSROOT /home/user/penchini_sysroot)
#set(CMAKE_FIND_ROOT_PATH /home/user/penchini_sysroot)

set(tools /usr/bin)
set(CMAKE_C_COMPILER ${tools}/aarch64-linux-gnu-gcc)
set(CMAKE_CXX_COMPILER ${tools}/aarch64-linux-gnu-g++)
set(CMAKE_CXX_LINKER  ${tools}/aarch64-linux-gnu-ld)

#set(tools /home/user/gcc-linaro-7.3.1-2018.05-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu-)
#set(CMAKE_C_COMPILER ${tools}gcc)
#set(CMAKE_CXX_COMPILER ${tools}g++)
#set(CMAKE_LINKER  ${tools}ld)

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
