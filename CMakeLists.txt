cmake_minimum_required(VERSION 3.10)

# project name
project(RTLS_demo)

# C++ standard
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)

if(DEFINED BUILD_PLATFORM)
set(CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} -D${BUILD_PLATFORM})
endif()

# Setup includes directories
set(OPENCV_DIR          ${CMAKE_SYSROOT}/usr/include/)
set(REALSENSE_DIR       libs/librealsense_arch64_r2_44_0/include/)
set(APRILTAG_DIR        libs/apriltag/include/)
# Setup libraries directories
if (CMAKE_SYSTEM_PROCESSOR STREQUAL aarch64)
#arm64 libraries
message("LINK ARM64")
set(LIB_APRILTAG        libs/apriltag/arm64/)
set(LIB_OPENCV          ${CMAKE_SYSROOT}/usr/lib/aarch64-linux-gnu/)
set(OTHER_LIBRARIES     ${CMAKE_SYSROOT}/usr/lib/aarch64-linux-gnu/tegra/ ${CMAKE_SYSROOT}/lib/aarch64-linux-gnu/ ${CMAKE_SYSROOT}/usr/lib/ ${CMAKE_SYSROOT}/usr/lib/aarch64-linux-gnu/blas/ ${CMAKE_SYSROOT}/usr/lib/aarch64-linux-gnu/lapack/)
set(LIB_REALSENSE       libs/librealsense_arch64_r2_44_0/lib/aarch64/)
else()
message("LINK X86")
#x86_64 libraries
#set(LIB_OPENCV          ${CMAKE_SYSROOT}/usr/lib/x86_64-linux-gnu/)
set(LIB_APRILTAG        libs/apriltag/x86_64/)
set(LIB_OPENCV          ${CMAKE_SYSROOT}/usr/local/lib/)
set(OTHER_LIBRARIES     ${CMAKE_SYSROOT}/lib/)
set(LIB_REALSENSE       /usr/local/lib/)
endif()

message("LINK TO ${LIB_OPENCV}")
# Add libraries paths
link_directories(${LIB_OPENCV})
#link_directories(${OTHER_LIBRARIES})
link_directories(${LIB_REALSENSE})
link_directories(${LIB_APRILTAG})

# Add executable (the target)

file(GLOB SRC 
    "src/*.h"
    "src/*.cc"
)

add_executable(rtls main.cc thread_utilities.cc ${SRC})

target_include_directories(rtls PUBLIC ${OPENCV_DIR})
target_include_directories(rtls PUBLIC ${REALSENSE_DIR})
target_include_directories(rtls PUBLIC ${APRILTAG_DIR})
target_include_directories(rtls PUBLIC "src/")

# Add link libraries
target_link_libraries(rtls pthread)

target_link_libraries(rtls opencv_imgproc)
target_link_libraries(rtls opencv_imgcodecs)
target_link_libraries(rtls opencv_highgui)
target_link_libraries(rtls opencv_core)
target_link_libraries(rtls opencv_videoio)
target_link_libraries(rtls opencv_video)
target_link_libraries(rtls opencv_objdetect)
target_link_libraries(rtls opencv_calib3d)
target_link_libraries(rtls apriltag)
target_link_libraries(rtls realsense2)
# NOTE 1
# target_link_libraries(ai_safe_crossing "-Wl,--start-group") adds the --start-group option to the linker command 
# line. It is needed because tensorflow is a static library and the linker tries to resolve symbols also in the 
# the libraries linked by tensorflow. This option has an high compilation performance cost, but there aren't 
# other options for cross compilation. The "-Wl," parameter says to the toolchain that this option is for
# the linker, not for the compiler.
