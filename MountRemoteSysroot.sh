#!/bin/bash
clear
echo " _   __ _            _  _                                      _    "
echo "| | / /(_)          (_)| |                                    | |   "
echo "| |/ /  _ __      __ _ | |_  _ __  ___   _ __    ___     _ __ | |   "
echo "|    \ | |\ \ /\ / /| || __|| '__|/ _ \ | '_ \  / __|   | '__|| |   "
echo "| |\  \| | \ V  V / | || |_ | |  | (_) || | | | \__ \ _ | | _ | | _ "
echo "\_| \_/|_|  \_/\_/  |_| \__||_|   \___/ |_| |_| |___/(_)|_|(_)|_|(_)"
echo "===================================================================="
echo "                   REMOTE SYSROOT MOUNT SCRIPT"

#HELP
if [ $# -gt 0 ]; then
    if  [ $1 == "-h" ] || [ $1 == "--help" ] ; then
    echo "Make sure that sshfs is installed, otherwise call: sudo apt install sshfs"
    echo "Usage: specify the mount point or the default one will be used"
    echo "Then, the script will ask you for the ip address of the remote device"
    echo "and for username and password of the remote user"
    echo "PLEASE NOTE: THIS SCRIPT NEEDS THE ROOT PRIVILEGES"
    exit
    fi
fi


#check if the script is running with root privileges
USER_ID=$(id -u) #get the user id. 0 means root

if [ $USER_ID -ne 0 ]; then
    echo "THE SCRIPT NEEDS ROOT PRIVILEGES"
    exit
fi

if [ $# -gt 0 ]; then
    if [ -d $1 ]; then
        SYSROOT_DIR=$1
    else
        echo "Mount directory $1 does not exist"
        exit
    fi
else
    echo "Using default mount point"
    SYSROOT_DIR="/ssh_sysroot/"
fi

echo "Mount point: $SYSROOT_DIR"

echo "Type di board ip address"
read IP_ADDRESS

echo "Type remote user"
read REM_USER

sudo sshfs -o allow_other,default_permissions,IdentityFile=/home/user/.ssh/id_rsa $REM_USER@$IP_ADDRESS:/ $SYSROOT_DIR







