#!/bin/bash
#-G "Eclipse CDT4 - Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug
#cmake -DCMAKE_TOOLCHAIN_FILE=CMake/toolchain_file.cmake -G "Eclipse CDT4 - Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ./

source common_build_functions.sh

function PrintMainScreen {
clear
echo " _   __ _            _  _                                      _    "
echo "| | / /(_)          (_)| |                                    | |   "
echo "| |/ /  _ __      __ _ | |_  _ __  ___   _ __    ___     _ __ | |   "
echo "|    \ | |\ \ /\ / /| || __|| '__|/ _ \ | '_ \  / __|   | '__|| |   "
echo "| |\  \| | \ V  V / | || |_ | |  | (_) || | | | \__ \ _ | | _ | | _ "
echo "\_| \_/|_|  \_/\_/  |_| \__||_|   \___/ |_| |_| |___/(_)|_|(_)|_|(_)"
echo "===================================================================="
echo "                     BUILD FOR ARM 64 SCRIPT"
} 

function BuildRelease {
    PrintMainScreen
    echo "Release build"
    cmake -DCMAKE_TOOLCHAIN_FILE=CMake/toolchain_file.cmake -G "Eclipse CDT4 - Unix Makefiles" -DCMAKE_BUILD_TYPE=Release -DOS_USER=kiwi ./ 
    make -j4
}

function BuildDebug {
    PrintMainScreen
    echo "Debug build"
    cmake -DCMAKE_TOOLCHAIN_FILE=CMake/toolchain_file.cmake -G "Eclipse CDT4 - Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -DOS_USER=kiwi ./ 
    make -j4
}

function Help {
    PrintMainScreen
    echo "Available parameters:"
    echo "-d or -D for Debug build type (also Eclipse projects will work with the same configuration)"
    echo "-r or -R for Release build type (also Eclipse projects will work with the same configuration)"
    echo "-c or -C for Clean all generated files/directories"
}

function Parameters_Error {
    echo "Wrong parameter(s). Use -h or --help for available options"
}


if [ $# -le 1 ]
then

    case $1 in
        "-d" | "-D")
            BuildDebug
        ;;
        "-c" | "-C")
            PrintMainScreen
            Clean
        ;;
        "-r" | "-R")
            BuildRelease
        ;;
        "-h" | "--help")
            Help
        ;;
        *)
            Parameters_Error
        ;;
    esac
elif [ $# -gt 1 ] 
then
    Parameters_Error
else
    BuildRelease
fi


