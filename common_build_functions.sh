#!/bin/bash

function Clean {
    echo "Project clean"

    #CMake temporary files
    if [ -d "CMakeFiles" ]; then
        rm -rf CMakeFiles
    fi

    if [ -f "CMakeCache.txt" ]; then
        rm CMakeCache.txt
    fi

    if [ -f "cmake_install.cmake" ]; then
        rm cmake_install.cmake
    fi
    #Generated Makefile

    if [ -f "Makefile" ]; then
        rm Makefile
    fi
    #Generated Eclipse project

    if [ -f ".cproject" ]; then
        rm .cproject 
    fi

    if [ -f ".project" ]; then
        rm .project
    fi

    #Built target
    if [ -f "motion_tracker" ]; then
        rm motion_tracker
    fi
}



