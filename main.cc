#include "frames_manager.h"
#include "thread_utilities.h"
#include <vector>
#include "pose_estimator.h"
#include "map_drawing.h"
#include "opencv2/imgcodecs.hpp"

#define PI 3.14159265
#define USE_FIXED_EXPOSURE true
#define FIXED_EXPOSURE 50.0f

void GenerateTagMatrix(Tag &tag, float map_x_meters, float map_z_meters, int y_axis_rotation_degs)
{
    tag.rt_matrix = cv::Matx44f::eye();

    //degs to rads
    float y_axis_rotation_rads = -y_axis_rotation_degs * PI / 180.0f;

    //rotate the tag
    tag.rt_matrix(0, 0) = cos(y_axis_rotation_rads);
    tag.rt_matrix(0, 2) = sin(y_axis_rotation_rads);
    tag.rt_matrix(2, 0) = -sin(y_axis_rotation_rads);
    tag.rt_matrix(2, 2) = cos(y_axis_rotation_rads);

    //translate the tag

    tag.rt_matrix(0, 3) = map_x_meters;
    tag.rt_matrix(2, 3) = map_z_meters;

    const cv::Vec4f origin{
        {0.f},
        {0.f},
        {0.f},
        {1.f}};

    tag.global_position = tag.rt_matrix * origin;
}

void GenerateOfficeMap(GlobalMap &map);
void GenerateHouseMap(GlobalMap &map);
void GenerateOfficeMap2(GlobalMap &map);
void GenerateProductionMap(GlobalMap &map);
void GenerateTestMap(GlobalMap &map);

int main(int argc, char **argv)
{

    GlobalMap map;

    //GenerateOfficeMap(map);


    unsigned long period = 0;
    unsigned int samples = 0;
    unsigned int frame_rate = 0;

    float X=0;
    float Y=0;
    unsigned int acq = 0;

    //GenerateOfficeMap2(map);

    //GenerateHouseMap(map);

    GenerateProductionMap(map);

    //GenerateTestMap(map);

    InitFrameManager(USE_FIXED_EXPOSURE, FIXED_EXPOSURE);

    InitEstimator();

    InitMapDrawingSystem(map);

    Camera camera;

    while (true)
    {

        cv::Mat frame;
        std::vector<ActiveTags> active_tags;
        GetFrame(frame);
        unsigned long start_tick = thread_utilities::GetTickCount();
        bool pose_estimated = EstimatePose(map, camera, frame, active_tags);
        period += thread_utilities::GetTickCount() - start_tick;
        samples++;
        if (samples >= 10)
        {
            period = period/(samples*1000);
            frame_rate = 1000/period;
            period = 0;
            samples = 0;
        }
        //std::cout << "fps=" << frame_rate << " on board=" << frame_rate/4 << std::endl;

        DrawMap(map, camera, active_tags, pose_estimated);
        if (pose_estimated)
        {
            X += camera.global_position(0, 0);
            Y += camera.global_position(2, 0);
            acq++;

            if (acq >= 10)
            {
                X /= acq;
                Y /= acq;
                acq = 0;
                std::cout << X << "\t" << Y << std::endl;
                X = 0;
                Y = 0;
            }
            //std::cout << camera.global_position(0, 0) << "\t" << camera.global_position(2, 0) << std::endl;
            //getchar();
        }
    }
    return 0;
}

void GenerateOfficeMap(GlobalMap &map)
{
    map.width = 3.535;
    map.height = 4.612;

    Tag tag2;

    tag2.id = 2;

    tag2.tag_size = 0.063;

    //set tag position and orientation
    GenerateTagMatrix(tag2, 3.534, 1.945, -90);

    //add to map
    map.tags.push_back(tag2);

    Tag tag3;

    tag3.id = 3;

    tag3.tag_size = 0.063;

    GenerateTagMatrix(tag3, 3.534, 3.432, -90);

    map.tags.push_back(tag3);

    Tag tag1;

    tag1.id = 1;

    tag1.tag_size = 0.063;

    GenerateTagMatrix(tag1, 2.106, 4.168, 0);

    map.tags.push_back(tag1);
}

void GenerateOfficeMap2(GlobalMap &map)
{
    map.width = 3.535;
    map.height = 4.612;

    Tag tag;

    tag.id = 10;

    tag.tag_size = 0.08;

    //set tag position and orientation
    GenerateTagMatrix(tag, 1.75, 4.612, 0);

    //add to map
    map.tags.push_back(tag);
}


void GenerateHouseMap(GlobalMap &map)
{
    map.width = 4.2;
    map.height = 3.49;

    Tag tag1;

    tag1.id = 1;

    tag1.tag_size = 0.064;

    //set tag position and orientation
    GenerateTagMatrix(tag1, 4.01, 0.13, -90);

    //add to map
    map.tags.push_back(tag1);

    tag1.id = 3;
    //add to map
    map.tags.push_back(tag1);

    Tag tag2;

    tag2.id = 2;

    tag2.tag_size = 0.064;

    //set tag position and orientation
    GenerateTagMatrix(tag2, 4.2, 2.13, -90);

    //add to map
    map.tags.push_back(tag2);

    tag2.id = 4;

    map.tags.push_back(tag2);
}


void GenerateProductionMap(GlobalMap &map)
{
    map.width = 39.69;
    map.height = 23.86;
    map.background = cv::imread("dwg_produzione.PNG", cv::IMREAD_UNCHANGED);

    Tag tag0;
    
    tag0.tag_size = 0.44;
    
    tag0.id = 1;

    GenerateTagMatrix(tag0, 25, 8.2, 0);
    map.tags.push_back(tag0);

    /*tag0.id = 1;

    GenerateTagMatrix(tag0, 26.9, 8.4, 0);

    map.tags.push_back(tag0);

    tag0.id = 2;
    GenerateTagMatrix(tag0, 29, 8.4, 0);

    map.tags.push_back(tag0);*/

}


void GenerateTestMap(GlobalMap &map)
{
    map.width = 16.0;
    map.height = 10.0;

    Tag tag;
    tag.id = 0;
    tag.tag_size = 0.08;

    GenerateTagMatrix(tag, 5, 5, 0);
    map.tags.push_back(tag);
    
    tag.id = 1;
    GenerateTagMatrix(tag, 7, 5, 0);
    map.tags.push_back(tag);

    tag.id = 2;
    GenerateTagMatrix(tag, 9, 5, 0);
    map.tags.push_back(tag);

    tag.id = 3;
    GenerateTagMatrix(tag, 11, 5, 0);
    map.tags.push_back(tag);
}