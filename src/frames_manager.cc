#include "frames_manager.h"
#include "opencv2/highgui.hpp"

static rs2::context ctx;
static rs2::pipeline pipeline;
static rs2::config cfg;
static rs2::frameset frameset;

cv::Mat camera_matrix;
cv::Mat dist_coeff;

#define DRAW_FRAME 1
#define READ_FROM_FILE 0
#define BAG_FILE_NAME "/home/user/Documents/20220303_092327_2.05sec.bag"
//#define BAG_FILE_NAME "/home/user/Documents/20220303_092532_2.04sec.bag"
//#define BAG_FILE_NAME "/home/user/Documents/20220303_091605_1.86sec.bag"

static bool using_fixed_exposure = true;

void get_intrinsics_(rs2::video_frame video_stream, cv::Mat &cameraMatrix, cv::Mat &distCoeff);

void InitFrameManager(bool use_fix_exposure, float exposure)
{
    #if !(READ_FROM_FILE)
    rs2::sensor rgb_camera;
    std::string device_id = "000000000000";

    // get device id of camera
    for (auto &&dev : ctx.query_devices()) // Query the list of connected RealSense devices
    {
        const char *serial = dev.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER);
        device_id = serial;
        std::cout << serial << std::endl;
        std::vector<rs2::sensor> sensors = dev.query_sensors();
        for (std::vector<rs2::sensor>::iterator it = sensors.begin(); it != sensors.end(); it++)
        {
            std::string camera_name((*it).get_info(RS2_CAMERA_INFO_NAME));
            if (camera_name.compare("RGB Camera") == 0)
            {
                rgb_camera = *it;
                break;
            }
        }
        break;
    }

    using_fixed_exposure = use_fix_exposure;

    if (use_fix_exposure)
    {
        rgb_camera.set_option(RS2_OPTION_ENABLE_AUTO_EXPOSURE, 0.0);
        rgb_camera.set_option(RS2_OPTION_EXPOSURE, exposure);
    }
    else
    {
        rgb_camera.set_option(RS2_OPTION_ENABLE_AUTO_EXPOSURE, 1.0);
    }

    #else



    #endif

    pipeline = rs2::pipeline(ctx);
#if !(READ_FROM_FILE)
    cfg.enable_device(device_id);
#else
    cfg.enable_device_from_file(BAG_FILE_NAME);
#endif
    //cfg.enable_stream(RS2_STREAM_DEPTH, 640, 480, RS2_FORMAT_Z16, 30);
    //cfg.enable_stream(RS2_STREAM_COLOR, 640, 360, RS2_FORMAT_RGB8, 60);
    cfg.enable_stream(RS2_STREAM_COLOR, 1280, 720, RS2_FORMAT_RGB8, 30);

    pipeline.start(cfg);

    frameset = pipeline.wait_for_frames();
    rs2::video_frame video_frame = frameset.get_color_frame();

    get_intrinsics_(video_frame, camera_matrix, dist_coeff);
}

void GetFrame(cv::Mat &color_frame)
{
    frameset = pipeline.wait_for_frames();
    rs2::video_frame video_frame = frameset.get_color_frame();
    cv::Mat original = cv::Mat(cv::Size(video_frame.get_width(), video_frame.get_height()), CV_8UC3, (void *)video_frame.get_data(), cv::Mat::AUTO_STEP);

    cv::Mat gray;
    cv::Mat resized = original(cv::Rect(0,original.rows/3, original.cols, original.rows/3));
    
    cv::cvtColor(resized, color_frame, cv::COLOR_RGB2GRAY);

    //cv::undistort(gray, color_frame, camera_matrix, dist_coeff);

    if (using_fixed_exposure)
    {
        cv::equalizeHist(color_frame, color_frame);
    }

#if DRAW_FRAME
    cv::imshow("equalized", color_frame);
    cv::waitKey(1);
#endif
}

void get_intrinsics_(rs2::video_frame video_stream, cv::Mat &cameraMatrix, cv::Mat &distCoeff)
{
    if (true)
    {
        rs2::video_stream_profile color_profile = video_stream.get_profile().as<rs2::video_stream_profile>();
        try
        {
            //If the stream is indeed a video stream, we can now simply call get_intrinsics()
            rs2_intrinsics intrinsics = color_profile.get_intrinsics();
            auto principal_point = std::make_pair(intrinsics.ppx, intrinsics.ppy);
            auto focal_length = std::make_pair(intrinsics.fx, intrinsics.fy);
            rs2_distortion model = intrinsics.model;

            std::cout << std::endl
                      << "Width: " << intrinsics.width << " Height: " << intrinsics.height << std::endl;
            std::cout << std::endl
                      << "Principal Point         : " << principal_point.first << ", " << principal_point.second << std::endl;
            std::cout << "Focal Length            : " << focal_length.first << ", " << focal_length.second << std::endl;
            std::cout << "Distortion Model        : " << model << std::endl;
            std::cout << "Distortion Coefficients : [" << intrinsics.coeffs[0] << "," << intrinsics.coeffs[1] << "," << intrinsics.coeffs[2] << "," << intrinsics.coeffs[3] << "," << intrinsics.coeffs[4] << "]" << std::endl;

            double focal = focal_length.first;
            cv::Point pp;
            pp.x = principal_point.first;
            pp.y = principal_point.second;

            cameraMatrix = (cv::Mat_<double>(3, 3) << focal_length.first, 0, pp.x, 0, focal_length.second, pp.y, 0, 0, 1);
            distCoeff = (cv::Mat_<double>(1, 5) << intrinsics.coeffs[0], intrinsics.coeffs[1], intrinsics.coeffs[2], intrinsics.coeffs[3], intrinsics.coeffs[4]);
        }
        catch (const std::exception &e)
        {
            std::cerr << "Failed to get intrinsics for the given stream. " << e.what() << std::endl;
        }
    }
}
