#pragma once

//#include "opencv2/imgcodecs.hpp"
#include "opencv2/core.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/video.hpp"
#include "opencv2/calib3d.hpp"
#include <librealsense2/rs.h>
#include <librealsense2/h/rs_types.h>
#include <librealsense2/rsutil.h>
#include <librealsense2/hpp/rs_export.hpp>
#include <librealsense2/hpp/rs_pipeline.hpp>

void InitFrameManager(bool use_fix_exposure, float exposure=0.0f);

void GetFrame(cv::Mat& color_frame);