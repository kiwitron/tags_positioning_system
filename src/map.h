#pragma once

#include <opencv2/core.hpp>
#include <vector>

class _3DObject
{
public:
    cv::Matx44f rt_matrix;
    cv::Matx41f global_position;
};

class Tag: public _3DObject
{
public:
    int id;
    float tag_size;
};

class ActiveTags: public Tag
{
public:
    float distance_from_code;
};

class Code
{
public:
    int id;
    std::vector<Tag> tags;
};

class Camera: public _3DObject
{
public:
    float y_rotation_degs;
    
};

class GlobalMap
{
public:
    float width;
    float height;

    cv::Mat background;

    std::vector<Tag> tags;
};