#include "map_drawing.h"
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#define MAP_PIXEL_MAX_SIZE 800
#define DRAW_X_AXIS 0

#define DRAW_POSITION_DELTAS 1

#if DRAW_POSITION_DELTAS

//DRAW THE DISTANCE RESPECT THE OLD POSITION

static cv::Matx41f old_position = {0, 0, 0, 0};
static unsigned int samples = 0;
static float distance_samples = 0.0f;
static float distance_avg = 0.0f;

#endif

static int graphic_map_width;
static int graphic_map_height;

void GetPixelPosition(int &x, int &y, GlobalMap &map, cv::Matx41f &real_position);

void InitMapDrawingSystem(GlobalMap &map)
{
    if (map.width > map.height)
    {
        graphic_map_width = MAP_PIXEL_MAX_SIZE;
        graphic_map_height = (map.height * MAP_PIXEL_MAX_SIZE) / map.width;
    }
    else
    {
        graphic_map_height = MAP_PIXEL_MAX_SIZE;
        graphic_map_width = (map.width * MAP_PIXEL_MAX_SIZE) / map.height;
    }
}

void DrawMap(GlobalMap &map, Camera &camera, std::vector<ActiveTags> &active_tags, bool pose_estimated)
{
    cv::Mat graphic_map;

    if (map.background.empty())
    {
        graphic_map = cv::Mat::zeros(cv::Size(graphic_map_width, graphic_map_height), CV_8UC3);
    }
    else
    {
        cv::resize(map.background, graphic_map, cv::Size(graphic_map_width, graphic_map_height));
    }

    //draw tags

    for (std::vector<Tag>::iterator it = map.tags.begin(); it != map.tags.end(); it++)
    {
        const cv::Vec4f tag_origin{
            {0.f},
            {0.f},
            {0.f},
            {1.f}};

        cv::Matx41f tag_position = it->rt_matrix * tag_origin;

        int x, y;

        GetPixelPosition(x, y, map, tag_position);

        cv::circle(graphic_map, cv::Point(x, y), 10, cv::Scalar(0, 255, 255)); //yellow circle

#if DRAW_X_AXIS
        int origin_x = x;
        int origin_y = y;

        const cv::Vec4f x_axis{
            {1.f},
            {0.f},
            {0.f},
            {1.f}};

        cv::Matx41f x_axis_new = it->rt_matrix * x_axis;

        GetPixelPosition(x, y, map, x_axis_new);

        cv::line(graphic_map, cv::Point(origin_x, origin_y), cv::Point(x, y), cv::Scalar(0, 0, 255));
#endif
    }

    if (pose_estimated)
    {
        //draw camera

        int camera_x, camera_y;

        GetPixelPosition(camera_x, camera_y, map, camera.global_position);

        cv::circle(graphic_map, cv::Point(camera_x, camera_y), 10, cv::Scalar(0, 0, 255)); //red circle

        //draw active tags
        for (std::vector<ActiveTags>::iterator it = active_tags.begin(); it != active_tags.end(); it++)
        {
            int x, y;

            GetPixelPosition(x, y, map, it->global_position);

            cv::circle(graphic_map, cv::Point(x, y), 11, cv::Scalar(0, 255, 0), 3);

            cv::line(graphic_map, cv::Point(camera_x, camera_y), cv::Point(x, y), cv::Scalar(255, 0, 0));
            cv::putText(graphic_map, std::to_string(it->distance_from_code) + "m", cv::Point((x + camera_x) / 2, (y + camera_y) / 2 - 10), cv::FONT_HERSHEY_PLAIN, 0.8, cv::Scalar(255, 0, 0));
        }

#if DRAW_POSITION_DELTAS
        
        distance_samples += sqrt(pow(camera.global_position(0,0) - old_position(0,0), 2) + pow(camera.global_position(2,0) - old_position(2,0), 2));
        samples++;

        if (samples >= 10)
        {
            distance_avg = distance_samples / samples;
            samples = 0;
            distance_samples = 0.0f;
        }

        cv::putText(graphic_map, std::to_string(distance_avg) + "m", cv::Point(0,10), cv::FONT_HERSHEY_PLAIN, 0.8, cv::Scalar(255, 0, 0));

        int x,y;
        GetPixelPosition(x, y, map, old_position);
        cv::line(graphic_map, cv::Point(camera_x, camera_y), cv::Point(x, y), cv::Scalar(255, 0, 0));
        old_position = camera.global_position;
#endif
    }
    cv::imshow("map", graphic_map);
    cv::waitKey(10);
}

void GetPixelPosition(int &x, int &y, GlobalMap &map, cv::Matx41f &real_position)
{
    x = (graphic_map_width * real_position(0, 0)) / map.width;
    y = (graphic_map_height * real_position(2, 0)) / map.height; //here we use z coordinate due the differenct coordinate system

    x = std::min(x, graphic_map_width - 1);
    y = graphic_map_height - std::min(y, graphic_map_height - 1); //the map zero is bottom left
}
