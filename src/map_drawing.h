#pragma once

#include "map.h"


void DrawMap(GlobalMap& map, Camera& camera, std::vector<ActiveTags>& active_tags, bool pose_estimated=true);
void InitMapDrawingSystem(GlobalMap& map);