#include "pose_estimator.h"
#include <apriltag.h>
#include <tag36h11.h>
#include <apriltag_pose.h>
#include <iostream>
#include "../thread_utilities.h"

static apriltag_family_t *tf;
static apriltag_detector_t *td;

class CameraPosition
{
public:
    cv::Matx41f position;
    float distance_from_code;
    float score;
};

void EvaluatePositionsScores(std::vector<CameraPosition> &camera_positions);
bool EstimateCameraPosition(Camera &camera, std::vector<CameraPosition> &camera_positions);

void InitEstimator()
{
    tf = tag36h11_create();

    td = apriltag_detector_create();

    apriltag_detector_add_family(td, tf);

    td->quad_decimate = 1.0f;
    td->quad_sigma = 0.0f;
    td->refine_edges = 1;
    td->nthreads = 2;
    td->debug = 0;
}

bool EstimatePose(GlobalMap &map, Camera &camera, cv::Mat frame, std::vector<ActiveTags>& active_tags)
{
    image_u8_t im = {.width = frame.cols,
                     .height = frame.rows,
                     .stride = frame.cols,
                     .buf = frame.data};

    zarray_t *detections = apriltag_detector_detect(td, &im);

    bool pose_estimed = false;

    std::vector<CameraPosition> camera_positions;

    for (int i = 0; i < zarray_size(detections); i++)
    {
        apriltag_detection_t *det;

        zarray_get(detections, i, &det);

        apriltag_detection_info_t info;

        info.det = det;

        if (frame.cols == 1280)
        {
            info.fy = 632.513;
            info.fx = 630.78;
            info.cx = 627.982;
            info.cy = 382.775;
            /*info.fy = 700;
            info.fx = 700;
            info.cx = 640;
            info.cy = 360;*/
        }
        else if (frame.cols == 640)
        {
            info.fy = 315;
            info.fx = 316;
            info.cx = 313;
            info.cy = 191;
        }
        else
        {
            std::cout << "FRAME SIZE NOT HANDLED" << std::endl;
            return false;
        }
        

        Tag current_tag;
        current_tag.id = -1; //invalid tag

        for (std::vector<Tag>::iterator it = map.tags.begin(); it != map.tags.end(); it++)
        {
            if (it->id == det->id)
            {
                current_tag = *it;
                break;
            }
        }

        if (current_tag.id == -1) //the tag is invalid
        {
            //this tag is not in the map
            continue;
        }

        info.tagsize = current_tag.tag_size;

        apriltag_pose_t pose;

        double err = estimate_tag_pose(&info, &pose);

        cv::Matx44f transformation; //transformation matrix of the tag respect the camera

        for (int row = 0; row < 3; row++)
        {
            for (int col = 0; col < 3; col++)
            {
                transformation(row, col) = pose.R->data[col + row * 3];
            }
        }

        for (int row = 0; row < 3; row++)
        {
            transformation(row, 3) = pose.t->data[row];
        }

        transformation(3, 3) = 1;

        const cv::Vec4f origin{
            {0.f},
            {0.f},
            {0.f},
            {1.f}};

        //Chirality check

        //std::cout << "X=" << current_tag.global_position(0,0) << " Y=" << current_tag.global_position(1,0) << " Z=" << current_tag.global_position(2,0) << " err=" << err << std::endl;

        cv::Matx41f code_respect_camera = transformation * origin;

        cv::Matx44f inverse_transformation = transformation.inv(); //transformation matrix of the camera respect the tag

        CameraPosition camera_position;

        camera_position.position = current_tag.rt_matrix * inverse_transformation * origin;

        bool code_on_the_right = false;

        float left_code_size = det->p[0][1] - det->p[3][1];
        float right_code_size = det->p[1][1] - det->p[2][1];

        cv::Matx41f camera_position_respect_code = inverse_transformation * origin;

        /*std::cout << "CODE X=" << code_respect_camera(0, 0) << " Y=" << code_respect_camera(1, 0) << " Z=" << code_respect_camera(2, 0) << " err=" << err << std::endl;
        std::cout << "CAMERA X=" << camera_position_respect_code(0, 0) << " Y=" << camera_position_respect_code(1, 0) << " Z=" << camera_position_respect_code(2, 0) << " err=" << err << std::endl;
        
        float beta = -asin(transformation(2,0));
        float alpha = asin(transformation(2,1)/cos(beta));
        float gamma = asin(transformation(1,0)/cos(beta));

        std::cout << "alpha=" << alpha*180.0f/3.14f << " beta=" << beta*180.0f/3.14f << " gamma=" << gamma*180.0f/3.14f << std::endl;

        beta = -asin(inverse_transformation(2,0));
        alpha = asin(inverse_transformation(2,1)/cos(beta));
        gamma = asin(inverse_transformation(1,0)/cos(beta));

        std::cout << "alpha=" << alpha*180.0f/3.14f << " beta=" << beta*180.0f/3.14f << " gamma=" << gamma*180.0f/3.14f << std::endl<< std::endl<< std::endl;*/

        /*for (int row = 0; row < 4; row++)
        {
            for (int col = 0; col < 4; col++)
            {
                std::cout << transformation(row, col) << " ";
            }
            std::cout << std::endl;
        }
        std::cout << "---------------------" << std::endl;
        for (int row = 0; row < 4; row++)
        {
            for (int col = 0; col < 4; col++)
            {
                std::cout << inverse_transformation(row, col) << " ";
            }
            std::cout << std::endl;
        }*/

        camera_position.distance_from_code = sqrt(pow(camera_position_respect_code(0, 0), 2) + pow(camera_position_respect_code(1, 0), 2) + pow(camera_position_respect_code(2, 0), 2));

        //camera_position.distance_from_code -= (camera_position.distance_from_code * 0.1);
        //camera_position.distance_from_code -= (camera_position.distance_from_code * 0.21);//code size = 0.1m

        //std::cout << "Distance = " << camera_position.distance_from_code  << std::endl;

        camera_positions.push_back(camera_position);

        ActiveTags active_tag;
        active_tag.id = current_tag.id;
        active_tag.tag_size = current_tag.tag_size;
        active_tag.global_position = current_tag.global_position;
        active_tag.distance_from_code = camera_position.distance_from_code;

        active_tags.push_back(active_tag);
    }

    EvaluatePositionsScores(camera_positions);

    return EstimateCameraPosition(camera, camera_positions);
}

void EvaluatePositionsScores(std::vector<CameraPosition> &camera_positions)
{
    /* 
    Distance reliability:
    D <= 1.3 -> 100%
    D > 1.3 < 2 -> 50% - 0%
    D >= 2 -> 0%
    */

    //const float high_accuracy_max_distance = 1.3;
    //const float med_accuracy_max_distance = 2.0;
    const float high_accuracy_max_distance = 1.6;
    const float med_accuracy_max_distance = 2.3;

    for (std::vector<CameraPosition>::iterator it = camera_positions.begin(); it != camera_positions.end(); it++)
    {
        if (it->distance_from_code < high_accuracy_max_distance)
        {
            it->score = 100;
        }
        else if (it->distance_from_code < med_accuracy_max_distance)
        {
            it->score = (20 / (med_accuracy_max_distance - high_accuracy_max_distance)) * (med_accuracy_max_distance - it->distance_from_code);
        }
        else
        {
            it->score = 0.01;
        }
    }
}

//#define USE_ALPHA_FILTER

static unsigned long last_tick = 0;

bool EstimateCameraPosition(Camera &camera, std::vector<CameraPosition> &camera_positions)
{
    float total_weight = 0;
    cv::Matx41f new_global_position;
    for (std::vector<CameraPosition>::iterator it = camera_positions.begin(); it != camera_positions.end(); it++)
    {
        total_weight += it->score;
        new_global_position += it->position * it->score;
    }

    new_global_position /= total_weight;

    if (total_weight > 0)
    {
        #ifdef USE_ALPHA_FILTER
        float kalman_gain = (total_weight / camera_positions.size())/100; //use the medium positions score as kalman gain (high kalman gain => trustable measure)

        camera.global_position = camera.global_position + kalman_gain * (new_global_position - camera.global_position);

        //std::cout << "KF=" << kalman_gain << std::endl;
        #else

        camera.global_position = new_global_position;

        #endif
        return true;
    }

    return false;
}
