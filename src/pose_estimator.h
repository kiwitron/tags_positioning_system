#pragma once

#include <opencv2/core.hpp>
#include "map.h"


void InitEstimator();
bool EstimatePose(GlobalMap& map, Camera& camera, cv::Mat frame, std::vector<ActiveTags>& active_tags);