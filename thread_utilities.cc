#include "thread_utilities.h"
#include <mutex>
#include <chrono>
#include <condition_variable>
#include <thread>

using namespace std;

namespace thread_utilities
{
    class c_thread_event: public thread_event
    {
    public:
        c_thread_event();
        wait_return wait_event  (int milliseconds_timeout);
        void        signal_event();

    private:
        bool IsSignaled();
        bool ResetSignaled();
        std::condition_variable         _condition_variable;
        std::mutex                      _mtx;
        std::unique_lock<std::mutex>    _lock_mutex;
        bool                            _signaled;
    };

    thread_event* CreateEvent()
    {
        return new c_thread_event();
    }

    void DestroyEvent(thread_event** event)
    {
        c_thread_event* cevent = (c_thread_event*)(*event);
        delete cevent;
        *event = nullptr;
    }

    wait_return WaitEvent(thread_event* event, unsigned int milliseconds_timeout)
    {
        c_thread_event* cevent = (c_thread_event*)event;
        return cevent->wait_event(milliseconds_timeout);
    }

    void SignalEvent (thread_event* event)
    {
        c_thread_event* cevent = (c_thread_event*)event;
        cevent->signal_event();
    }

    void Sleep_ms(int milliseconds)
    {
        std::this_thread::sleep_for(chrono::milliseconds(milliseconds));
    }

    void Sleep_us(int microseconds)
    {
        std::this_thread::sleep_for(chrono::microseconds(microseconds));
    }

    namespace {
        chrono::steady_clock::time_point _s_start_tick = chrono::steady_clock::now();
    }

    unsigned long int GetTickCount()
    {
        return (unsigned long int)chrono::duration_cast<chrono::microseconds>(chrono::steady_clock::now() - _s_start_tick).count();
    }

    c_thread_event::c_thread_event():
    _mtx(), 
    _lock_mutex(_mtx, std::defer_lock),
    _signaled(false)
    {
        
    }

    wait_return c_thread_event::wait_event(int milliseconds_timeout)
    {
        if (milliseconds_timeout == INFINITE_TIMEOUT)
        {
            _lock_mutex.lock();
            _condition_variable.wait(_lock_mutex, [this]{ return _signaled; });
            _signaled = false;
            _lock_mutex.unlock();
        }
        else
        {
            chrono::microseconds check_interval = chrono::duration_cast<chrono::microseconds>(chrono::milliseconds(milliseconds_timeout));
            _lock_mutex.lock();
            bool ret = _condition_variable.wait_for(_lock_mutex, check_interval, [this]{ return _signaled; });
            if (ret)
            {
            	_signaled = false;
            }
            _lock_mutex.unlock();
            if (!ret)
            {
                return wait_return::event_timeout;
            }
        }
        return wait_return::event_signaled;
    }

    void c_thread_event::signal_event()
    {
        _mtx.lock();
        _signaled = true;
        _mtx.unlock();
        _condition_variable.notify_one();
    }

    bool c_thread_event::IsSignaled()
    {
        return _signaled;
    }

} // namespace thread_utilities
