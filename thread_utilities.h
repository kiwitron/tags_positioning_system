#ifndef _THREAD_UTILITIES_HEADER_
#define _THREAD_UTILITIES_HEADER_


namespace thread_utilities
{
    class thread_event
    {
    };

    enum class wait_return
    {
        event_signaled,
        event_timeout
    };

    const unsigned int INFINITE_TIMEOUT = -1; //UINT MAX

    thread_event*       CreateEvent ();
    void                DestroyEvent(thread_event** event);
    wait_return         WaitEvent   (thread_event* event, unsigned int milliseconds_timeout);
    void                SignalEvent (thread_event* event);
    unsigned long int   GetTickCount();
    void                Sleep_ms    (int milliseconds);
    void                Sleep_us    (int microseconds);
}



#endif //_THREAD_UTILITIES_HEADER_
